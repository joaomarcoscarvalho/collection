import { FcNext, FcPrevious } from "react-icons/fc";
import { IconButton } from "@material-ui/core";
import { Container } from "./styles";
const PageBar = ({ page, next, previous }) => {
  return (
    <Container>
      <IconButton onClick={() => previous()}>
        <FcPrevious />
      </IconButton>
      <span>{page}</span>
      <IconButton onClick={() => next()}>
        <FcNext />
      </IconButton>
    </Container>
  );
};

export default PageBar;
