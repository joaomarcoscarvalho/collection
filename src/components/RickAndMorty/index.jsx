import {
  makeStyles,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
} from "@material-ui/core";
import { Container } from "./styles";
import { BsStar } from "react-icons/bs";

import { useState, useEffect } from "react";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: "100vw",
    maxWidth: "100%",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

const RickAndMorty = ({ page, arrFavoritesRickAndMorty, value }) => {
  const classes = useStyles();
  const [char, setChar] = useState([]);

  const makeFav = (e, char) => {
    const alreadyExists = arrFavoritesRickAndMorty.find(
      (element) => element.id === char.id
    );
    if (alreadyExists) return;
    arrFavoritesRickAndMorty.push(char);
    window.localStorage.setItem(
      "rickandmorty",
      JSON.stringify(arrFavoritesRickAndMorty)
    );
    e.target.classList.add("true");
  };

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/?page=${page}`)
      .then((res) => setChar(res.data.results));
  }, [page]);

  useEffect(() => {
    if (value) {
      axios
        .get(`https://rickandmortyapi.com/api/character/?name=${String(value)}`)
        .then((res) => setChar(res.data.results));
    }
  }, [value]);

  // setChar(searched);
  return (
    <Container className={classes.root}>
      <GridList cellHeight={200} className={classes.gridList}>
        {char.map((tile, index) => {
          return (
            <GridListTile key={index}>
              <img src={tile.image} alt={tile.name} />
              <GridListTileBar
                title={tile.name}
                actionIcon={
                  <IconButton
                    aria-label={`info about ${tile.name}`}
                    className={classes.icon}
                    onClick={(e) => {
                      makeFav(e, char[index], index);
                    }}
                  >
                    <BsStar />
                  </IconButton>
                }
              />
            </GridListTile>
          );
        })}
      </GridList>
    </Container>
  );
};

export default RickAndMorty;
