import {
  makeStyles,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
} from "@material-ui/core";
import { BsStar } from "react-icons/bs";
import { Container } from "./styles";
import { useState, useEffect } from "react";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: "100vw",
    maxWidth: "100%",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));
const Pokemon = ({ page, arrFavoritesPokemon, value }) => {
  const classes = useStyles();
  const [chars, setChars] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const makeFav = (e, char) => {
    const alreadyExists = arrFavoritesPokemon.find(
      (element) => element.name === char.name
    );
    if (alreadyExists) return;
    arrFavoritesPokemon.push(char);
    window.localStorage.setItem("pokemon", JSON.stringify(arrFavoritesPokemon));
    return e.target.classList.add("true");
  };

  useEffect(() => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon/?offset=${page}&limit=150`)
      .then((res) => {
        setChars(res.data.results);
        setFiltered(res.data.results);
      });
  }, [page]);

  useEffect(() => {
    // if (value) {
    //   axios
    //     .get(`https://pokeapi.co/api/v2/pokemon/${value}`)
    //     .then((res) => setChars([res.data]))
    //     .catch((err) => console.log("Pokemon não existe"));
    // }
    if (chars.length > 0) {
      const allPokemons = [...chars];
      console.log(allPokemons);
      const searched = allPokemons.filter((pokemon) =>
        pokemon.name?.toLowerCase().includes(value.toLowerCase())
      );
      console.log(searched);
      if (value) return setChars(searched);
      if (value === "") return setChars(filtered);
    }
  }, [value]);

  return (
    <Container className={classes.root}>
      <GridList cellHeight={200} className={classes.gridList}>
        {chars?.map((tile, index) => {
          let brokenUrl;
          let id;
          if (tile.url) {
            brokenUrl = tile.url.split("/");
            id = brokenUrl[brokenUrl?.length - 2];
          }
          return (
            <GridListTile key={index}>
              <img
                src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
                  tile.id ? tile.id : id
                }.png`}
                alt={tile.name}
              />
              <GridListTileBar
                title={tile.name}
                actionIcon={
                  <IconButton
                    aria-label={`info about ${tile.name}`}
                    className={classes.icon}
                    onClick={(e) => makeFav(e, chars[index])}
                  >
                    <BsStar />
                  </IconButton>
                }
              />
            </GridListTile>
          );
        })}
      </GridList>
    </Container>
  );
};

export default Pokemon;
