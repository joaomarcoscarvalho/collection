import styled from "styled-components";

export const Container = styled.div`
  @media (max-width: 425px) {
    .toolbar {
      font-size: 0.5rem;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
  }
`;
