import { makeStyles } from "@material-ui/core";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Input,
} from "@material-ui/core";
import { GiDiamondsSmile } from "react-icons/gi";
import { Container } from "./styles";
import { useHistory } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const Header = ({ poChanged, rmChanged, setValue, value }) => {
  const history = useHistory();
  const classes = useStyles();
  return (
    <Container className={classes.root}>
      <AppBar position="static">
        <Toolbar className="toolbar">
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={() => history.push("/")}
          >
            <GiDiamondsSmile />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Collections
          </Typography>
          <Input
            value={value}
            onChange={(e) => setValue(e.target.value)}
            inputProps={{ "aria-label": "description" }}
          />
          <Button color="inherit" onClick={() => poChanged()}>
            Pokemon
          </Button>
          <Button color="inherit" onClick={() => rmChanged()}>
            Rick and Morty
          </Button>
          <Button
            color="inherit"
            onClick={() => history.push("/favorites/pokemon")}
          >
            Pokemon Favoritos
          </Button>
          <Button
            color="inherit"
            onClick={() => history.push("/favorites/rickandmorty")}
          >
            Rick And Morty Favoritos
          </Button>
        </Toolbar>
      </AppBar>
    </Container>
  );
};
export default Header;
