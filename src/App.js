import Routes from "./Routes";
import Header from "./components/Header";
import { useState } from "react";
// import "./App.css";

function App() {
  const rickAndMortyFavorites = [];
  const pokemonFavorites = [];
  const [value, setValue] = useState("");
  const [change, setChange] = useState(false);

  const poChanged = () => {
    return setChange(true);
  };

  const rmChanged = () => {
    return setChange(false);
  };

  return (
    <div className="App">
      <header className="App-header">
        <Header
          poChanged={poChanged}
          rmChanged={rmChanged}
          setValue={setValue}
          value={value}
        />
        <Routes
          rickAndMortyFavorites={rickAndMortyFavorites}
          pokemonFavorites={pokemonFavorites}
          change={change}
          value={value}
        />
      </header>
    </div>
  );
}

export default App;
