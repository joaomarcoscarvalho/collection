import { Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import PoFavorites from "../pages/PoFavorites";
import RmFavorites from "../pages/RmFavorites";
const Routes = ({ rickAndMortyFavorites, pokemonFavorites, value, change }) => {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Home
            rickAndMortyFavorite={rickAndMortyFavorites}
            pokemonFavorites={pokemonFavorites}
            change={change}
            value={value}
          />
        </Route>
        <Route path="/favorites/pokemon">
          <PoFavorites pokemonFavorites={pokemonFavorites} />
        </Route>
        <Route path="/favorites/rickandmorty">
          <RmFavorites rickAndMortyFavorites={rickAndMortyFavorites} />
        </Route>
      </Switch>
    </div>
  );
};

export default Routes;
