import {
  makeStyles,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
} from "@material-ui/core";
import { Container } from "./styles";
import { GrClose } from "react-icons/gr";

import { useState, useEffect } from "react";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: "100vw",
    maxWidth: "100%",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

const PoFavorites = ({ pokemonFavorites }) => {
  const classes = useStyles();
  const [fav, setFav] = useState(pokemonFavorites);

  useEffect(() => {
    setFav(fav);
  }, [fav]);

  useEffect(() => {
    const localFav = window.localStorage.getItem("pokemon");
    if (localFav) {
      setFav(JSON.parse(localFav));
    }
  }, []);

  const removeElement = (index) => {
    const remove = [...fav];
    pokemonFavorites.splice(index, 1);
    remove.splice(index, 1);
    window.localStorage.setItem("pokemon", JSON.stringify(remove));
    setFav(remove);
  };

  return (
    <div>
      <Container className={classes.root}>
        <GridList cellHeight={200} className={classes.gridList}>
          {fav?.map((tile, index) => {
            const brokenUrl = tile.url.split("/");
            const id = brokenUrl[brokenUrl.length - 2];
            return (
              <GridListTile key={index}>
                <img
                  src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
                  alt={tile.name}
                />
                <GridListTileBar
                  title={tile.name}
                  actionIcon={
                    <IconButton
                      aria-label={`info about ${tile.name}`}
                      className={classes.icon}
                      onClick={() => removeElement(index)}
                    >
                      <GrClose />
                    </IconButton>
                  }
                />
              </GridListTile>
            );
          })}
        </GridList>
      </Container>
    </div>
  );
};

export default PoFavorites;
