import {
  makeStyles,
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
} from "@material-ui/core";
import { Container } from "./styles";
import { GrClose } from "react-icons/gr";

import { useState, useEffect } from "react";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "column",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: "100vw",
    maxWidth: "100%",
  },
  icon: {
    color: "rgba(255, 255, 255, 0.54)",
  },
}));

const RmFavorites = ({ rickAndMortyFavorites }) => {
  const classes = useStyles();
  // const newFav = [];

  const [fav, setFav] = useState(rickAndMortyFavorites);

  useEffect(() => {
    setFav(fav);
  }, [fav]);

  useEffect(() => {
    // const newLocalFav = [...new Set(JSON.parse(localFav))];
    const localFav = window.localStorage.getItem("rickandmorty");
    if (localFav) {
      setFav(JSON.parse(localFav));
    }
  }, []);

  const removeElement = (index) => {
    rickAndMortyFavorites.splice(index, 1);
    const removed = [...fav];
    removed.splice(index, 1);
    window.localStorage.setItem("rickandmorty", JSON.stringify(removed));
    setFav(removed);
  };
  return (
    <div>
      <Container className={classes.root}>
        <GridList cellHeight={200} className={classes.gridList}>
          {fav?.map((tile, index) => {
            return (
              <GridListTile key={index}>
                <img src={tile.image} alt={tile.name} />
                <GridListTileBar
                  title={tile.name}
                  actionIcon={
                    <IconButton
                      aria-label={`info about ${tile.name}`}
                      className={classes.icon}
                      onClick={(e) => removeElement(index)}
                    >
                      <GrClose />
                    </IconButton>
                  }
                />
              </GridListTile>
            );
          })}
        </GridList>
      </Container>
    </div>
  );
};

export default RmFavorites;
