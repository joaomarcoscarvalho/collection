import Pokemon from "../../components/Pokemon";
import RickAndMorty from "../../components/RickAndMorty";
import PageBar from "../../components/PageBar";
import { useState } from "react";

const Home = ({ rickAndMortyFavorite, pokemonFavorites, change, value }) => {
  const [page, setPage] = useState(1);

  const next = () => {
    return setPage(page + 1);
  };

  const previous = () => {
    return page > 1 ? setPage(page - 1) : null;
  };

  return (
    <div>
      <PageBar page={page} next={next} previous={previous} />
      {change ? (
        <Pokemon
          page={page}
          arrFavoritesPokemon={pokemonFavorites}
          value={value}
        />
      ) : (
        <RickAndMorty
          page={page}
          arrFavoritesRickAndMorty={rickAndMortyFavorite}
          value={value}
        />
      )}
    </div>
  );
};

export default Home;
